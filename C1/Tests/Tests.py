import unittest

from ProblemLogic import SimpleSearchProcs
from Utils import BinaryTree

class SimpleSearchProcsTests(unittest.TestCase):
    def test_hamming_dist(self):
        arr1 = [1, 2, 3, 4]
        arr2 = [5, 2, 7, 6]
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.hamming_dist(arr1, arr2), 3)
        arr1 = arr2
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.hamming_dist(arr1, arr2), 0)
        arr1 = []
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.hamming_dist(arr1, arr2), -1)
        arr2 = []
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.hamming_dist(arr1, arr2), 0)
        arr1 = [None] * 5
        arr2 = [5] * 5
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.hamming_dist(arr1, arr2), len(arr1))

    def test_manhattan_dist(self):
        arr1 = [1, 2, 3, 4]
        arr2 = [5, 2, 7, 6]
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.manhattan_dist(arr1, arr2), 10)
        arr1 = []
        self.assertRaises(Exception, SimpleSearchProcs.SimpleSearchProcs.manhattan_dist(arr1, arr2), 33)
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.manhattan_dist(arr1, arr2), -1)
        arr1 = [1]
        arr2 = [2]
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.manhattan_dist(arr1, arr2), 1)
        arr1 = [1, 3, 4]
        arr2 = [1, 3, 4]
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.manhattan_dist(arr1, arr2), 0)

    def test_levenshtein_distance(self):
        arr1 = [1, 2, 3, 4]
        arr2 = [5, 2, 4]
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.levenshtein_dist(arr1, arr2), 2)
        arr1 = "kitten"
        arr2 = "sitting"
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.levenshtein_dist(arr1, arr2), 3)
        arr1 = "k"
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.levenshtein_dist(arr1, arr2), len(arr2))
        arr1 = []
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.levenshtein_dist(arr1, arr2), 7)
        arr2 = []
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.levenshtein_dist(arr1, arr2), 0)

    def test_bfs(self):
        # build binary tree from the bottom up
        leaf_1 = BinaryTree.BinaryNode(3)
        leaf_2 = BinaryTree.BinaryNode(5)
        node_1 = BinaryTree.BinaryNode(4, leaf_1, leaf_2)
        node_2 = BinaryTree.BinaryNode(7)
        root = BinaryTree.BinaryNode(6, node_1, node_2)

        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.bfs(root), [6, 4, 7, 3, 5])
        self.assertRaises(Exception, SimpleSearchProcs.SimpleSearchProcs.bfs(root), [6, 4, 7, 3, 5])
        root = None
        self.assertRaises(Exception, SimpleSearchProcs.SimpleSearchProcs.bfs(root), [6, 4, 7, 3, 5])
        root = BinaryTree.BinaryNode(3)
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.bfs(root), [3])
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.bfs(node_1), [4, 3, 5])
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.bfs(leaf_1), [3])

    def test_dfs(self):
        # build binary tree from the bottom up
        leaf_1 = BinaryTree.BinaryNode(3)
        leaf_2 = BinaryTree.BinaryNode(5)
        node_1 = BinaryTree.BinaryNode(4, leaf_1, leaf_2)
        node_2 = BinaryTree.BinaryNode(7)
        root = BinaryTree.BinaryNode(6, node_1, node_2)

        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.dfs(root), [6, 4, 3, 5, 7])
        root = None
        self.assertRaises(Exception, SimpleSearchProcs.SimpleSearchProcs.dfs(root), [6, 4, 3, 5, 7])
        root = BinaryTree.BinaryNode(3)
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.dfs(root), [3])
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.dfs(node_1), [4, 3, 5])
        self.assertEqual(SimpleSearchProcs.SimpleSearchProcs.dfs(leaf_1), [3])

if __name__ == '__main__':
    unittest.main()
