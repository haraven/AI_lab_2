from Utils.BinaryTree import *
import math


class SimpleSearchProcs:
    # Computes the Hamming distance between two arrays
    @staticmethod
    def hamming_dist(arr1, arr2):
        if len(arr1) != len(arr2):
            return -1
        counter = 0
        for index in range(len(arr1)):
            if arr1[index] != arr2[index]:
                counter += 1

        return counter

    # Computes the Manhattan distance between two arrays
    @staticmethod
    def manhattan_dist(arr1, arr2):
        if len(arr1) != len(arr2):
            return -1
        manhattan_sum = 0
        for index in range(len(arr1)):
            manhattan_sum += abs(arr1[index] - arr2[index])

        return manhattan_sum

    # Used by the Levenshtein distance function.
    @staticmethod
    def indicator(char1, char2):
        if char1 == char2:
            return 0
        else:
            return 1

    # auxiliary function for the Levenshtein distance.
    @staticmethod
    def lev(arr1, arr2, pos_i, pos_j):
        if min(pos_i, pos_j) == 0:
            return max(pos_i, pos_j)
        return min(
            SimpleSearchProcs.lev(arr1, arr2, pos_i - 1, pos_j) + 1,
            SimpleSearchProcs.lev(arr1, arr2, pos_i, pos_j - 1) + 1,
            SimpleSearchProcs.lev(arr1, arr2, pos_i - 1, pos_j - 1) +
            SimpleSearchProcs.indicator(arr1[pos_i], arr2[pos_j])
        )

    # Computes the Levenshtein distance between two arrays.
    @staticmethod
    def levenshtein_dist(arr1, arr2):
        if len(arr1) == 0:
            return len(arr2)
        elif len(arr2) == 0:
            return len(arr1)
        return 1 + SimpleSearchProcs.lev(arr1, arr2, len(arr1) - 1, len(arr2) - 1)

    # Computes the BFS array, starting at a given node.
    @staticmethod
    def bfs(root):
        if root is None:
            return []
        queue = [root]
        visited = set()
        res = []
        while len(queue) != 0:
            node = queue.pop(0)
            if node not in visited:
                res.append(node.val)
                if not (node.left_child is None):
                    queue.append(node.left_child)
                if not (node.right_child is None):
                    queue.append(node.right_child)
                visited.add(node)
        return res

    # Computes the DFS array, starting at a given node.
    @staticmethod
    def dfs(root):
        if root is None:
            return []
        stack = [root]
        visited = set()
        res = []
        while len(stack) != 0:
            node = stack.pop()
            if node not in visited:
                res.append(node.val)
                if not (node.right_child is None):
                    stack.append(node.right_child)
                if not (node.left_child is None):
                    stack.append(node.left_child)
                visited.add(node)

        return res
