from Utils.ConsistencyChecker import *
from Sudoku import Board


class SudokuBoardConsistencyChecker(ConsistencyChecker):
    """Sudoku board consistency checking class."""

    def __init__(self):
        pass

    # checks whether the board is in a consistent state (i.e. a solution may be reached from it)
    def is_consistent(self, obj):
        if type(obj) is not Board.Board:
            return False

        slots = obj.get_slots()
        grid_size = obj.get_grid_size()

        for row in range(grid_size):
            for col in range(grid_size):
                if not obj.is_slot_valid(slots[row][col]):
                    return False

        return True

    # checks whether the board is a valid solution (i.e. it is consistent and it has no empty cells)
    def is_solution(self, obj):
        if type(obj) is not Board.Board:
            return False

        row, col = obj.get_first_empty_slot()
        if self.is_consistent(obj):
            return row == -1 or col == -1

        return False
