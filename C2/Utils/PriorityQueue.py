
class PriorityQueue(object):
    """Priority queue class"""

    def __init__(self, order=min, f=lambda x: x):
        self.__queue = []
        self.__order = order
        self.__f = f

    def append(self, item):
        self.__queue.append(item)
        self.__queue.sort(key=lambda x: self.__f(x))

    def __len__(self):
        return len(self.__queue)

    def pop(self):
        if self.__order == min:
            return self.__queue.pop(0)
        else:
            return self.__queue.pop()

    def extend(self, items):
        for item in items:
            self.append(item)
