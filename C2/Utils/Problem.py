from abc import ABCMeta, abstractmethod


class Problem(object):
    """Abstract problem class"""

    __metaclass__ = ABCMeta

    @abstractmethod
    def setup_children(self):
        pass
