from abc import ABCMeta, abstractmethod


class ConsistencyChecker(object):
    """Abstract class for checking the consistency of an object"""
    __metaclass__ = ABCMeta

    @abstractmethod
    def is_consistent(self, obj):
        pass

    @abstractmethod
    def is_solution(self, obj):
        pass
