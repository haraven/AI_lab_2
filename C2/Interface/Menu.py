from abc import ABCMeta, abstractmethod


class UI(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def parse(self):
        pass


class MenuItem(UI):
    def __init__(self, name):
        self.__name = name

    def parse(self):
        print(self.__name)


class Menu(UI):
    """Menu class. Stores a list of menu items"""

    def __init__(self):
        self.__items = []

    def parse(self):
        for item in self.__items:
            item.parse()

        return self

    def append(self, item):
        if not issubclass(item.__class__, UI):
            raise TypeError("Item: " + item + " is not a subclass of UI.")

        self.__items.append(item)

        return self
