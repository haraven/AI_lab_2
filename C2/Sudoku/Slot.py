import copy


class Slot(object):
    """Represents a single Sudoku table slot."""

    def __init__(self, row, col, val, is_final=False):
        self.__row = row
        self.__col = col
        self.__val = val
        self.__is_final = is_final

    def get_row(self):
        return self.__row

    def get_col(self):
        return self.__col

    def get_val(self):
        return self.__val

    def is_final(self):
        return self.__is_final

    def set_val(self, val):
        if not self.__is_final:
            self.__val = val

        return self

    def set_is_final(self, is_final):
        self.__is_final = is_final

        return self

    def clone(self):
        tmp = Slot(copy.deepcopy(self.__row), copy.deepcopy(self.__col), copy.deepcopy(self.__val),
                   copy.deepcopy(self.__is_final))

        return tmp
