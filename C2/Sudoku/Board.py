import copy
from math import *
from Sudoku import Slot
from Trees import Tree, Node
from Searches import SearchMethod
from Utils import Problem, SudokuBoardConsistencyChecker


class Board(Problem.Problem):
    """Sudoku board"""

    def __init__(self, filename=None):
        self.__board_tree = None
        self.__filled_counter = 0
        if filename is None:
            return

        file = open(filename)
        line = file.readline()
        tmp = int(line)
        if sqrt(tmp) != float(int(sqrt(tmp))):
            raise IOError("Grid size must be a square value.")
            return
        self.__grid_size = int(line)

        self.__slots = [[None for x in range(self.__grid_size)] for x in range(self.__grid_size)]
        for i in range(self.__grid_size):
            line = file.readline()
            values = line.split(', ')
            if len(values) < self.__grid_size:
                self.__slots = None
                self.__grid_size = 0
            for j in range(self.__grid_size):
                val = int(values[j])
                if val != 0:
                    self.__filled_counter += 1
                tmp = Slot.Slot(i, j, val, val != 0)
                self.__slots[i][j] = tmp

        root = Node.Node(self.clone())
        self.__board_tree = Tree.Tree(root)

    def get_slots(self):
        return self.__slots

    def get_filled_counter(self):
        return self.__filled_counter

    def get_grid_size(self):
        return self.__grid_size

    # search for sqrt grid by coords
    def __get_slot_grid_coords_by_pos(self, row, col):
        grid_sqrt = int(sqrt(self.__grid_size))
        addition_tuple = (0, grid_sqrt)

        for i in range(grid_sqrt + 1):
            square_a = (i * grid_sqrt, 0)  # top left
            square_b = (square_a[0] + addition_tuple[0], square_a[1] + addition_tuple[1])  # top right
            square_c = (grid_sqrt * (1 + i), 0)  # bottom left
            square_d = (square_c[0] + addition_tuple[0], square_c[1] + addition_tuple[1])  # bottom right

            while (square_a[0] < self.__grid_size and square_a[1] < self.__grid_size) and (
                            square_c[0] < self.__grid_size and square_c[1] < self.__grid_size):
                if square_b[0] <= row < square_d[0] and col < square_d[1]:
                    return square_a, square_b, square_c, square_d
                else:
                    square_a = copy.copy(square_b)
                    square_c = copy.copy(square_d)
                    square_b = (square_a[0] + addition_tuple[0], square_a[1] + addition_tuple[1])
                    square_d = (square_c[0] + addition_tuple[0], square_a[1] + addition_tuple[1])

        return (0, 0), (0, 0), (0, 0), (0, 0)

    def is_slot_valid(self, slot):
        # initial slots are always valid
        if slot.is_final() or slot.get_val() == 0:
            return True

        # check for duplicates on row and column
        for j in range(self.__grid_size):
            if j != slot.get_col():
                if self.__slots[slot.get_row()][j].get_val() == slot.get_val():
                    return False
            if j != slot.get_row():
                if self.__slots[j][slot.get_col()].get_val() == slot.get_val():
                    return False

        # check for duplicates in the sqrt grid of that slot
        square_a, square_b, square_c, square_d = self.__get_slot_grid_coords_by_pos(slot.get_row(), slot.get_col())
        for i in range(square_a[0], square_d[0]):
            for j in range(square_a[1], square_d[1]):
                current = self.__slots[i][j]
                if (i != slot.get_row() and j != slot.get_col()) and current != slot:
                    if current.get_val() == slot.get_val():
                        return False
        return True

    # retrieve the first empty slot on the board (parsing goes top to bottom, left to right)
    def get_first_empty_slot(self):
        for i in range(self.__grid_size):
            for j in range(self.__grid_size):
                if self.__slots[i][j].get_val() == 0:
                    return i, j

        return -1, -1

    def get_values_for_slot(self, row, col):
        # assume all values are available for that cell
        res = [x for x in range(1, self.__grid_size + 1)]

        # remove any value that is already present on the same row, or column
        for j in range(self.__grid_size):
            if self.__slots[row][j].get_val() in res:
                res.remove(self.__slots[row][j].get_val())
        for i in range(self.__grid_size):
            if self.__slots[i][col].get_val() in res:
                res.remove(self.__slots[i][col].get_val())

        # remove any value that is already present on the same sqrt grid as the one the slot is in
        square_a, square_b, square_c, square_d = self.__get_slot_grid_coords_by_pos(row, col)
        for i in range(square_a[0], square_d[0]):
            for j in range(square_a[1], square_d[1]):
                if (i != row and j != col) and self.__slots[i][j].get_val() in res:
                    res.remove(self.__slots[i][j].get_val())

        return res

    def clone(self):
        cloned_slots = [[None for x in range(self.__grid_size)] for x in range(self.__grid_size)]
        for row in range(self.__grid_size):
            for col in range(self.__grid_size):
                cloned_slots[row][col] = self.__slots[row][col].clone()

        cloned_board = Board()
        cloned_board.__slots = cloned_slots
        cloned_board.__grid_size = self.__grid_size
        cloned_board.__board_tree = self.__board_tree

        return cloned_board

    def setup_children(self):
        row, col = self.get_first_empty_slot()
        if row == -1 or col == -1:
            return

        nodes = []
        values = self.get_values_for_slot(row, col)
        for value in values:
            new_board = self.clone()
            new_board.__slots[row][col].set_val(value)
            new_board.__filled_counter = self.__filled_counter + 1
            node = Node.Node(new_board)
            nodes.append(node)
            # self.__board_tree.add_child(node)
            # new_board.__setup_candidates()

        return nodes

    def solve(self, search_method):
        if issubclass(search_method.__class__, SearchMethod.SearchMethod):
            # nodes = self.setup_children()
            # self.__board_tree.add_children(nodes)
            consistency_checker = SudokuBoardConsistencyChecker.SudokuBoardConsistencyChecker()
            res = search_method.solve(self.__board_tree, consistency_checker)
            return res
        return None

    def print_board(self):
        for row in range(self.__grid_size):
            for col in range(self.__grid_size):
                slot = self.__slots[row][col]
                if col < self.__grid_size - 1:
                    if slot.is_final():
                        print('\033[93m' + str(slot.get_val()), end=", ")
                    else:
                        print('\033[0m' + str(slot.get_val()), end=", ")
                elif slot.is_final():
                    print('\033[93m' + str(slot.get_val()))
                else:
                    print('\033[0m' + str(slot.get_val()))
        print("\033[0m-------------------------")
