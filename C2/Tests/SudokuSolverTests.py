import unittest
import random
import math
from Sudoku import Board
from Searches import BreadthFirstSearch, GreedyBestFirstSearch
from Trees import Node


def h(k):
    if not issubclass(k.__class__, Node.Node):
        return math.inf

    val = k.get_value()
    if not issubclass(val.__class__, Board.Board):
        return math.inf

    return val.get_grid_size() - val.get_filled_counter()


class MyTestCase(unittest.TestCase):
    def test_bfs_solution(self):
        board = Board.Board("../resources/test_board_4x4.txt")
        slots = board.get_slots()
        self.assertEqual(len(slots), 4)
        bfs = BreadthFirstSearch.BreadthFirstSearch()
        res = board.solve(bfs)
        self.assertRaises(Exception, res, None)
        slots = res.get_slots()
        self.assertEqual(len(slots[0]), 4)  # assert length is the same
        self.assertEqual(slots[0][0].get_val(), 3)  # default node, value is always the same
        self.assertNotEqual(slots[random.randint(0, 3)][random.randint(0, 3)].get_val(),
                            0)  # assert that a random position in the grid is not 0
        res_2 = board.solve(bfs)
        rand_row = random.randint(0, 3)
        rand_col = random.randint(0, 3)
        # a well-formed Sudoku board has a unique solution
        self.assertEqual(res.get_slots()[rand_row][rand_col].get_val(),
                         res_2.get_slots()[rand_row][rand_col].get_val())

    def test_gbfs_solution(self):
        board = Board.Board("../resources/test_board_4x4.txt")
        slots = board.get_slots()
        self.assertEqual(len(slots), 4)
        gbfs = GreedyBestFirstSearch.GreedyBestFirstSearch(h)
        res = board.solve(gbfs)
        self.assertRaises(Exception, res, None)
        slots = res.get_slots()
        self.assertEqual(len(slots[0]), 4)  # assert length is the same
        self.assertEqual(slots[0][0].get_val(), 3)  # default node, value is always the same
        self.assertNotEqual(slots[random.randint(0, 3)][random.randint(0, 3)].get_val(),
                            0)  # assert that a random position in the grid is not 0
        res_2 = board.solve(gbfs)

        rand_row = random.randint(0, 3)
        rand_col = random.randint(0, 3)
        # a well-formed Sudoku board has a unique solution
        self.assertEqual(res.get_slots()[rand_row][rand_col].get_val(),
                         res_2.get_slots()[rand_row][rand_col].get_val())


if __name__ == '__main__':
    unittest.main()
