
class Tree(object):
    """Tree class. Contains a root node."""

    def __init__(self, root=None):
        self.__root = root

    def get_root(self):
        return self.__root

    def set_root(self, root):
        self.__root = root

        return self

    def add_child(self, node):
        self.__root.add_child(node)

    def add_children(self, nodes):
        self.__root.add_children(nodes)
