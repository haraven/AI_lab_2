
class Node(object):
    """Tree node class. Has a value and an associated list of child nodes."""

    def __init__(self, value=None, children=None):
        if children is None:
            children = []
        self.__value = value
        self.__children = children.copy()

    def get_value(self):
        return self.__value

    def set_value(self, new_val):
        self.__value = new_val

        return self

    def get_children(self):
        return self.__children

    def set_children(self, new_children):
        self.__children = new_children

        return self

    def add_child(self, child):
        self.__children.append(child)

        return self

    def add_children(self, children):
        self.__children.extend(children)
