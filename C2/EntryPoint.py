import sys
import math
import time
import datetime
from Searches import BreadthFirstSearch, GreedyBestFirstSearch
from Trees import Node
from Sudoku import Board
from Interface import Menu


def h(k):
    if not issubclass(k.__class__, Node.Node):
        return math.inf

    val = k.get_value()
    if not issubclass(val.__class__, Board.Board):
        return math.inf

    return val.get_grid_size() - val.get_filled_counter()


def setup_UI(my_menu):
    if not issubclass(my_menu.__class__, Menu.Menu):
        raise TypeError("Cannot initialize " + my_menu + " as it is not a recognized menu.")

    item = Menu.Menu()
    item.append(Menu.MenuItem("- choose an input file for the Sudoku board (file);"))
    item.append(Menu.MenuItem(
        "\t a Sudoku input file must be of the form:"
        "\n\n\t\033[91msize_n"
        "\n\tcell00, cell01, ..., cell0n"
        "\n\t..."
        "\n\tcelln0, celln1, ..., cellnn\033[0m\n"))
    my_menu.append(item)
    item = Menu.MenuItem("- solve;")
    my_menu.append(item)
    item = Menu.MenuItem("- exit.")
    my_menu.append(item)

    return my_menu


def setup_solution_menu(my_menu):
    if not issubclass(my_menu.__class__, Menu.Menu):
        raise TypeError("Cannot initialize " + my_menu + " as it is not a recognized menu.")

    item = Menu.MenuItem("- breadth-first search (bfs);")
    my_menu.append(item)
    item = Menu.MenuItem("- greedy best-first search (gbfs).")
    my_menu.append(item)

    return my_menu


def handle_input(my_menu):
    board = None
    solution_menu = setup_solution_menu(Menu.Menu())

    print("Welcome. Please choose one of the below options: ")
    while True:
        try:
            my_menu.parse()
            cmd = input("> ")
            if cmd == "file":
                filename = input("Enter a file to read from:\n> ")
                board = Board.Board(filename)
            elif cmd == "solve":
                if board is None:
                    raise SyntaxError(
                        "Sudoku board is currently empty. You have to initialize it before trying to solve anything.\n")
                else:
                    solution_menu.parse()
                    cmd = input("> ")
                    if cmd == "bfs":
                        search_method = BreadthFirstSearch.BreadthFirstSearch()
                    elif cmd == "gbfs":
                        search_method = GreedyBestFirstSearch.GreedyBestFirstSearch(h)
                    else:
                        raise SyntaxError("invalid command.")

                    start = time.time()
                    res = board.solve(search_method)
                    end = time.time()
                    start_datetime = datetime.datetime.fromtimestamp(start)
                    end_datetime = datetime.datetime.fromtimestamp(end)
                    if res is not None:
                        print("---------")
                        print("Solution:")
                        print("---------")
                        res.print_board()
                    else:
                        print("There is no solution.")
                    print("Started at: " + str(start_datetime.strftime('%H:%M:%S')) + ". Ended at: " + str(
                        end_datetime.strftime('%H:%M:%S')) + ". Duration: " + str(end_datetime - start_datetime) + ".")
            elif cmd == "exit":
                print("The application will now exit. Have a good one!")
                sys.exit(0)
            else:
                raise SyntaxError("Invalid command")
        except Exception as e:
            print("Unexpected error: " + str(e))


if __name__ == "__main__":
    menu = setup_UI(Menu.Menu())
    handle_input(menu)
