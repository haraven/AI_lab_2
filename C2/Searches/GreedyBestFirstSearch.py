from builtins import super

from Searches.BestFirstSearch import *


class GreedyBestFirstSearch(BestFirstSearch):
    """GBFS class. Extends InformedSearchMethod."""

    def __init__(self, h):
        super(GreedyBestFirstSearch, self).__init__(h)

    # attempts to find a solution using GBFS, for the given tree.
    # A consistency checker is used to check the consistency of a node (and implicitly, its children),
    #  and whether or not we have reached a solution.
    def solve(self, tree, consistency_checker):
        return super(GreedyBestFirstSearch, self).solve(tree, consistency_checker)
