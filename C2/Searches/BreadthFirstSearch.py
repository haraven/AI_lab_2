from Searches.UninformedSearchMethod import *
from Utils import Problem

class BreadthFirstSearch(UninformedSearchMethod):
    """BFS class. Extends UninformedSearchMethod."""

    def __init__(self, tree=None):
        self.__queue = []
        if tree is not None:
            self.__queue.append(tree.get_root())

    # attempts to find a solution using BFS, for the given tree.
    #  A consistency checker is used to check the consistency of a node (and implicitly, its children),
    # and whether or not we have reached a solution.
    def solve(self, tree, consistency_checker):
        queue = [tree.get_root()]

        while len(queue) > 0:
            node = queue.pop(0)
            val = node.get_value()
            if issubclass(val.__class__, Problem.Problem):
                if not consistency_checker.is_consistent(val):
                    continue
                elif consistency_checker.is_solution(val):
                    return val
                else:
                    nodes = val.setup_children()
                    node.add_children(nodes)

                queue.extend(node.get_children())

        return None

    @staticmethod
    def parse(self, tree):
        result = []

        queue = [tree.get_root()]

        while len(queue) > 0:
            node = queue.pop(0)
            result.append(node.get_value())

            node_children = node.get_children()
            for child in node_children:
                queue.append(child)

        return result

    # will go to the next logical step in the BFS algorithm, for the tree given as argument to the class
    def one_step(self):
        if len(self.__queue) == 0:
            return None

        current = self.__queue.pop(0)
        node_children = current.get_children()
        for child in node_children:
            self.__queue.append(child)

        return current.get_value()

    def set_tree(self, tree):
        self.__queue.append(tree.get_root())
