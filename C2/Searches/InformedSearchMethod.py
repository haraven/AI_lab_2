from abc import ABCMeta, abstractmethod
from Searches.SearchMethod import SearchMethod


class InformedSearchMethod(SearchMethod):
    """Informed search method base class"""

    __metaclass__ = ABCMeta

    @abstractmethod
    def solve(self, tree, consistency_checker):
        pass

    @staticmethod
    @abstractmethod
    def parse(self, tree):
        pass
